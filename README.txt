About
=====
Integrates the fullPage.js library into Drupal.



About fullPage.js
----------------

Library available at http://alvarotrigo.com/fullPage

A simple and easy to use plugin to create fullscreen scrolling websites (also known as single page websites or onepage sites). It allows the creation of fullscreen scrolling websites, as well as adding some landscape sliders inside the sections of the site.

Installation
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [fullPage.js Library](http://alvarotrigo.com/fullPage)

Tasks
-----

1. Download the fullPage.js library from https://github.com/alvarotrigo/fullpage.js/archive/master.zip
2. Unzip the file and rename the folder to "fullpage" (pay attention to the case of the letters)
3. Put the folder in a libraries directory
    - Ex: sites/all/libraries
4. The following files are required (last files is required for javascript debugging)
    - dist/jquery.fullpage.min.js
    - dist/jquery.fullpage.min.css
    - dist/jquery.fullpage.js
    - dist/jquery.fullpage.css
5. Ensure you have a valid path similar to this one for all files
    - Ex: sites/all/libraries/fullpage/dist/jquery.fullpage.min.js

That's it!
