/**
 * @file
 * Aply Full Page.
 */
(function ($) {

  function fullpageAddClass(fpElement) {
    $(fpElement).addClass('fullpage-container');
  }

  Drupal.behaviors.fullpage = {
    attach: function (context, settings) {
      settings.fullpage = settings.fullpage || {};
      $.each(settings.fullpage, function(ind, iteration) {
        if (iteration.selectors.length) {
          fullpageAddClass(this.selectors);
        }
        // Navigation.
        var navigationMenu = iteration.navigation_menu;
        var navigationLock = iteration.navigation_lock;
        var navigationAnchors = iteration.navigation_anchors;
        var navigationNav = iteration.navigation_nav;
        var navigationPosition = iteration.navigation_position;
        var navigationTooltips = iteration.navigation_tooltips;
        var navigationTooltipsActive = iteration.navigation_tooltips_active;
        var navigationSlidesNav = iteration.navigation_slides_nav;
        var navigationSlidesPosition = iteration.navigation_slides_position;
        //Scrolling.
        var scrollingCcs3 = iteration.scrolling_css3;
        var scrollingSpeed = iteration.scrolling_speed;
        var scrollingAuto = iteration.scrolling_auto;
        var scrollingFit = iteration.scrolling_fit;
        var scrollingFitDelay = iteration.scrolling_fit_delay;
        var scrollingScrollbar = iteration.scrolling_scrollbar;
        var scrollingEasing = iteration.scrolling_easing;
        var scrollingEasingCss3 = iteration.scrolling_easing_css3;
        var scrollingLoobBottom = iteration.scrolling_loop_bottom;
        var scrollingLoopTop = iteration.scrolling_loop_top;
        var scrollingLoopHorizontal = iteration.scrolling_loop_horizontal;
        var scrollingContinuous = iteration.scrolling_continuous;
        var scrollingNormalScroll = iteration.scrolling_normal_scroll;
        var scrollingOverflow = iteration.scrolling_overflow;
        var scrollingTouch = iteration.scrolling_touch;
        var scrollingTouchTreshold = iteration.scrolling_touch_threshold;
        var scrollingDestination = iteration.scrolling_destination;
        $('.fullpage-container').fullpage({
          // Navigation.
          menu: navigationMenu,
          lockAnchors: navigationLock,
          anchors: navigationAnchors,
          navigation: navigationNav,
          navigationPosition: navigationPosition,
          navigationTooltips: navigationTooltips,
          showActiveTooltip: navigationTooltipsActive,
          slidesNavigation: navigationSlidesNav,
          slidesNavPosition: navigationSlidesPosition,
          // Scrolling.
          css3: scrollingCcs3,
          scrollingSpeed: scrollingSpeed,
          autoScrolling: scrollingAuto,
          fitToSection: scrollingFit,
          fitToSectionDelay: scrollingFitDelay,
          scrollBar: scrollingScrollbar,
          easing: scrollingEasing,
          easingcss3: scrollingEasingCss3,
          loopBottom: scrollingLoobBottom,
          loopTop: scrollingLoopTop,
          loopHorizontal: scrollingLoopHorizontal,
          continuousVertical: scrollingContinuous,
          normalScrollElements: scrollingNormalScroll,
          scrollOverflow: scrollingOverflow,
          // scrollOverflowOptions: null,
          touchSensitivity: scrollingTouch,
          normalScrollElementTouchThreshold: scrollingTouchTreshold,
          bigSectionsDestination: scrollingDestination
        });
      });

    }
  }

}(jQuery));
