<?php

/**
* @file
* Functionality for Fullpage.
*/

/**
 * Admin settings form for which page to aply fullPage.js.
 */
function fullpage_admin_form($form, &$form_state) {
  $form['fullpage_css_selectors'] = array(
    '#type' => 'textarea',
    '#title' => t('Selectors for which part of page create full page'),
    '#default_value' => variable_get('fullpage_css_selectors', ''),
    '#description' => t('Enter CSS/jQuery selectors of html object to create full page.  Comma separated or 1 per line'),
    '#required' => TRUE
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'fullpage_admin_form_submit';

  return $form;
}

/**
 * Submit handler for fullpage_admin_form.
 */
function fullpage_admin_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}


/**
 * Navigation settings form for which page to aply fullPage.js.
 */
function fullpage_navigation_admin_form($form, &$form_state) {
  // $form['navigation']= array(
  //   '#type' => 'fieldset',
  //   '#title' => t('Navigation Settings'),
  //   '#collapsible' => TRUE,
  //   '#collapsed' => TRUE,
  // );
  $form['navigation']['fullpage_navigation_menu'] = array(
    '#type' => 'textarea',
    '#title' => t('Selectors for navigations'),
    '#default_value' => variable_get('fullpage_navigation_menu', ''),
    '#description' => t('Enter CSS/jQuery selectors of html object to create full page.  Comma separated or 1 per line'),
  );
  $form['navigation']['fullpage_navigation_lockAnchors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock Anchors'),
    '#default_value' => variable_get('fullpage_navigation_lockAnchors', ''),
    '#description' => t('Default: False'),
  );
  $form['navigation']['fullpage_navigation_anchors'] = array(
    '#type' => 'textarea',
    '#title' => t('Selectors for navigations'),
    '#default_value' => variable_get('fullpage_navigation_anchors', ''),
    '#description' => t('Enter CSS/jQuery selectors of html object to create full page.  Comma separated or 1 per line'),
  );
  $form['navigation']['fullpage_navigation_navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation'),
    '#default_value' => variable_get('fullpage_navigation_navigation', ''),
    '#description' => t('Default: False'),
  );
  $form['navigation']['fullpage_navigation_navigationPosition'] = array(
    '#type' => 'radios',
    '#title' => t('Navigation Position'),
    '#options' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    '#default_value' => variable_get('fullpage_navigation_navigationPosition', 'right'),
    '#description' => t('Default: False'),
  );
  $form['navigation']['fullpage_navigation_navigationTooltips'] = array(
    '#type' => 'textarea',
    '#title' => t('Navigation Tooltips'),
    '#default_value' => variable_get('fullpage_navigation_navigationTooltips', ''),
    '#description' => t('Enter CSS/jQuery selectors of html object to create full page.  Comma separated or 1 per line'),
  );
  $form['navigation']['fullpage_navigation_activeTooltips'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active Tooltips'),
    '#default_value' => variable_get('fullpage_navigation_activeTooltips', ''),
    '#description' => t('Default: False'),
  );
  $form['navigation']['fullpage_navigation_slidesNav'] = array(
    '#type' => 'checkbox',
    '#title' => t('Slides Navigation'),
    '#default_value' => variable_get('fullpage_navigation_slidesNav', TRUE),
    '#description' => t('Default: True'),
  );
  $form['navigation']['fullpage_navigation_slidesPosition'] = array(
    '#type' => 'radios',
    '#title' => t('Slides Position'),
    '#options' => array(
      'bottom' => t('Bottom'),
      'top' => t('Top'),
    ),
    '#default_value' => variable_get('fullpage_navigation_slidesPosition', 'bottom'),
    '#description' => t('Default: False'),
  );


  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'fullpage_navigation_admin_form_submit';

  return $form;
}

/**
 * Submit handler for fullpage_navigation_admin_form.
 */
function fullpage_navigation_admin_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Scrolling settings form for which page to aply fullPage.js.
 */
function fullpage_scrolling_admin_form($form, &$form_state) {
  $form['scrolling']['fullpage_scrolling_css3'] = array(
    '#type' => 'checkbox',
    '#title' => t('css3'),
    '#default_value' => variable_get('fullpage_scrolling_css3', TRUE),
    '#description' => t('Default: True'),
  );
  $form['scrolling']['fullpage_scrolling_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Scrolling speed'),
    '#default_value' => variable_get('fullpage_scrolling_speed', '700'),
    '#description' => t('Enter the speed for the animation in ms. Only numbers.'),
  );
  $form['scrolling']['fullpage_scrolling_auto'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto scrolling'),
    '#default_value' => variable_get('fullpage_scrolling_auto', TRUE),
    '#description' => t('Default: True'),
  );
  $form['scrolling']['fullpage_scrolling_fit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fit to section'),
    '#default_value' => variable_get('fullpage_scrolling_fit', TRUE),
    '#description' => t('Default: True'),
  );
  $form['scrolling']['fullpage_scrolling_fit_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Fit to section delay'),
    '#default_value' => variable_get('fullpage_scrolling_fit_delay', '1000'),
    '#description' => t('Enter the speed for the delay in ms. Only numbers.'),
  );
  $form['scrolling']['fullpage_scrolling_scrollbar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scrollbar'),
    '#default_value' => variable_get('fullpage_scrolling_scrollbar', FALSE),
    '#description' => t('Show scrollbar. Default: False'),
  );
  $form['scrolling']['fullpage_scrolling_easing'] = array(
    '#type' => 'radios',
    '#title' => t('Easing'),
    '#options' => array(
      'easeInOutCubic' => t('easeInOutCubic'),
      'Sine' => t('Sine'),
      'Circ' => t('Circ'),
      'Elastic' => t('Elastic'),
      'Back' => t('Back'),
      'Bounce' => t('Bounce'),
    ),
    '#default_value' => variable_get('fullpage_scrolling_easing', 'easeInOutCubic'),
    '#description' => t('(default easeInOutCubic) Defines the transition effect to use for the vertical and horizontal scrolling.'),
  );
  $form['scrolling']['fullpage_scrolling_easing_css3'] = array(
    '#type' => 'radios',
    '#title' => t('Easing css3'),
    '#options' => array(
      'ease' => t('ease'),
      'ease-in' => t('ease-in'),
      'ease-out' => t('ease-out'),
      'ease-in-out' => t('ease-in-out'),
      'linear' => t('linear'),
      'cubic-bezier' => t('cubic-bezier'),
    ),
    '#default_value' => variable_get('fullpage_scrolling_easing_css3', 'ease'),
    '#description' => t(' (default ease) Defines the transition effect to use in case of using css3:true.'),
  );
  $form['scrolling']['fullpage_scrolling_loop_bottom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loop bottom'),
    '#default_value' => variable_get('fullpage_scrolling_loop_bottom', FALSE),
    '#description' => t('(default false) Defines whether scrolling down in the last section should scroll to the first one or not.'),
  );
  $form['scrolling']['fullpage_scrolling_loop_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loop top'),
    '#default_value' => variable_get('fullpage_scrolling_loop_top', FALSE),
    '#description' => t('(default false) Defines whether scrolling up in the first section should scroll to the last one or not.'),
  );
  $form['scrolling']['fullpage_scrolling_loop_horizontal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loop horizontal'),
    '#default_value' => variable_get('fullpage_scrolling_loop_horizontal', FALSE),
    '#description' => t('(default true) Defines whether horizontal sliders will loop after reaching the last or previous slide or not.'),
  );
  $form['scrolling']['fullpage_scrolling_continuous'] = array(
    '#type' => 'checkbox',
    '#title' => t('Continuous vertical'),
    '#default_value' => variable_get('fullpage_scrolling_loop_continuous', FALSE),
    '#description' => t('(default true) Defines whether horizontal sliders will loop after reaching the last or previous slide or not.'),
  );
  $form['scrolling']['fullpage_scrolling_normal_scroll'] = array(
    '#type' => 'textarea',
    '#title' => t('Normal scroll elements'),
    '#default_value' => variable_get('fullpage_scrolling_normal_scroll', ''),
    '#description' => t('If you want to avoid the auto scroll when scrolling over some elements, this is the option you need to use. (useful for maps, scrolling divs etc.) It requires a string with the jQuery selectors for those elements.'),
  );
  $form['scrolling']['fullpage_scrolling_overflow'] = array(
    '#type' => 'checkbox',
    '#title' => t('scroll overflow'),
    '#default_value' => variable_get('fullpage_scrolling_overflow', FALSE),
    '#description' => t('(default false) defines whether or not to create a scroll for the section/slide in case its content is bigger than the height of it. When set to true, your content will be wrapped by the plugin. Consider using delegation or load your other scripts in the afterRender callback.'),
  );
  $form['scrolling']['fullpage_scrolling_touch'] = array(
    '#type' => 'textfield',
    '#title' => t('Touch sensitivity'),
    '#default_value' => variable_get('fullpage_scrolling_touch', '15'),
    '#description' => t('Defines a percentage of the browsers window width/height, and how far a swipe must measure for navigating to the next section / slide.'),
  );
  $form['scrolling']['fullpage_scrolling_touch_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Normal Scroll Element Touch Threshold'),
    '#default_value' => variable_get('fullpage_scrolling_touch_threshold', '5'),
    '#description' => t('Defines the threshold for the number of hops up the html node tree Fullpage will test to see if normalScrollElements is a match to allow scrolling functionality on divs on a touch device.'),
  );
  $form['scrolling']['fullpage_scrolling_destination'] = array(
    '#type' => 'radios',
    '#title' => t('Big Sections Destination'),
    '#options' => array(
      'null' => t('None'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    ),
    '#default_value' => variable_get('fullpage_scrolling_destination', 'null'),
    '#description' => t('Defines how to scroll to a section which size is bigger than the viewport. By default fullPage.js scrolls to the top if you come from a section above the destination one and to the bottom if you come from a section below the destination one.'),
  );


  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'fullpage_scrolling_admin_form_submit';

  return $form;
}

/**
 * Submit handler for fullpage_scrolling_admin_form.
 */
function fullpage_scrolling_admin_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}
